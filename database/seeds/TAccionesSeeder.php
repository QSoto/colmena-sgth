<?php
/**
*@author Victor Hidalgo @konh
*/
use Illuminate\Database\Seeder;
class TAccionesSeeder extends Seeder
{
  public function run(){
    $action = array(
        array('nombre' => 'roles.registrar', 'navegacion' => true),
        array('nombre' => 'roles.modificar', 'navegacion' => false),
        array('nombre' => 'roles.listar', 'navegacion' => true),
        array('nombre' => 'roles.eliminar', 'navegacion' => false),

        array('nombre' => 'usuarios.registrar', 'navegacion' => true),
        array('nombre' => 'usuarios.modificar', 'navegacion' => false),
        array('nombre' => 'usuarios.listar', 'navegacion' => true),
        array('nombre' => 'usuarios.eliminar', 'navegacion' => false),

        array('nombre' => 'tareas.registrar', 'navegacion' => true),
        array('nombre' => 'tareas.modificar', 'navegacion' => false),
        array('nombre' => 'tareas.listar', 'navegacion' => true),
        array('nombre' => 'tareas.eliminar', 'navegacion' => false),

        array('nombre' => 'permisos_y_reposos.eliminar', 'navegacion' => false),
        array('nombre' => 'permisos_y_reposos.registrar', 'navegacion' => true),
        array('nombre' => 'permisos_y_reposos.modificar', 'navegacion' => false),
        array('nombre' => 'permisos_y_reposos.listar', 'navegacion' => true)

        //array('nombre' => 'actividades.registrar'),
        //array('nombre' => 'actividades.modificar'),
        //array('nombre' => 'actividades.listar'),
        //array('nombre' => 'actividades.eliminar'),
        );
    DB::table('t_acciones')->insert($action);
  }
}
