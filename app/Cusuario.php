<?php
/**
* @author QSoto
* @author EliasDP. @tesoner
*/
namespace Colmena;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Cusuario extends Authenticatable
{
    protected $table = "t_usuarios";
    protected $primaryKey = "idUsu";
    private $rolActivo = null;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cedula', 'username', 'nombres', 'apellidos',
    'tipUsu', 'email', 'clave', 'telefono', 'fecNac', 'sexo'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'clave', 'remember_token',
    ];
    public function getAuthPassword(){
        return $this->clave;
    }/*
    public function getAuthIdentifier(){
        return $this->username;
    }*/
    public function roles()
    {
        return $this->belongsToMany('Colmena\Crol', 't_role_usuas', 'idUsu', 'idRol');
    }

    public function tareas(){
    	return $this->hasMany('Colmena\Ctarea');
    }/*

    public function TPermRepo(){
    	return $this->hasMany('app\TPermRepo');
    }

    public function TEncargados(){
    	return $this->hasMany('app\TEncargados');
    }*/
    public function tieneRol($oRol){
        foreach ($this->roles as $iRol) {
            if($oRol->idRol == $iRol->idRol)
                return true;
        }
        return false;
    }
    public function tieneRolPorNombre($nombreRol){
        foreach ($this->roles as $iRol) {
            if($nombreRol == $iRol->nombre)
                return true;
        }
        return false;
    }

    public function getGradoOcupacion(){

        $grado=0;
        if ($this->tareas) {
            foreach ($this->tareas as $tarea) {
                if ($tarea->estTar!='Cumplida' or $tarea->estTar!='Cancelada')
                    $grado+=($tarea->complejidad)+($tarea->prioridad);
            }
        return $grado;
        }
    }
    public function getNombreCompleto(){
        return $this->nombres." ".$this->apellidos;
    }
    public function getMesNacimiento($nombre = false){
        $fechaNac = explode("-", $this->fecNac);
        if($nombre){
            $nombres = ["Enero", "Febrero", "Marzo", "Abril",
                        "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
                        "Octubre", "Noviembre", "Diciembre"];
            return $nombres[$fechaNac[1]-1];
        }
        return $fechaNac[1];
    }
    public function getDiaNacimiento(){
        $fechaNac = explode("-", $this->fecNac);
        return $fechaNac[2];
    }
    public function getUrl(){
        return "/usuarios/ver/".$this->idUsu;
    }
    /**
     * @param Recibe como parametro el nombre de una accion
     * @return Retorna True si alguno de los roles disponibles tiene disponible la accion recibida por parametro
     */
    public function tieneAccion($nombreAccion){
        foreach($this->roles as $rol)
            if($rol->tieneAccion($nombreAccion))
                return true;
        return false;
    }
}
