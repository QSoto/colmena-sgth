<?php
/*
Author:QSoto
*/
namespace Colmena;

use Illuminate\Database\Eloquent\Model;

class Ctarea extends Model
{
    protected $table = "t_tareas";
    protected $primaryKey = "idTar";
    //public $timestamps = false;
    public function BitacorasTarea(){
    	return $this->hasMany('Colmena\TBitaTare');
    }

    public function usuarioResponsable(){
        return $this->belongsTo('Colmena\Cusuario');
    }
    public function getURL(){
        return "/tareas/ver/".$this->idTar;
    }
}
