<?php
/*
Author:QSoto
*/
namespace Colmena;

use Illuminate\Database\Eloquent\Model;

class TCalendario extends Model
{
    protected $table = "TCalendario";
    protected $primarykey = "fecLab";
}
