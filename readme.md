# Colmena - Sistema de Gestión de Talento Humano (SGTH)


## Descripción:

SISTEMA AUTOMATIZADO PARA LA GESTIÓN DEL TALENTO HUMANO DEL DEPARTAMENTO DE PROGRAMA NACIONAL DE FORMACION EN INFORMATICA EN LA UNIVERSIDAD POLITÉCNICA TERRITORIAL ANDRÉS ELOY BLANCO


## Documentación:

* [Informe del Proyecto][507e4e67]  
* [Especificación de Requisito de Software (SRS)][0e226033]  
* [Manual de Usuario PDF][9030221f]
* [Wiki (Manual de Usuario en Línea)][554b66de]

  [507e4e67]: https://docs.google.com/document/d/1VQ2A0gzgTp4raKBAiwCNRUCl_5GhKQ5mQFI5X7e82XE/edit?usp=sharing "Informe del Proyecto"
  [0e226033]: https://docs.google.com/document/d/1Iw5N11WvvKNrpj1l_dXa1q6-5aWRJ54mLsQ1GDoVFZk/edit?usp=sharing "Especificación de Requisito de Software (SRS)"
  [9030221f]: # "Manual de Usuario PDF"
  [554b66de]: https://bitbucket.org/tesoner/colmena-sgth/wiki/Home "Manual de Usuario en Línea"


## Autores:

* **Elias Peraza** _eliasperaza0511@gmail.com_
* **Quintin Soto** _quintin.soto96@gmail.com_
* **Victor Hidalgo** _pkzkon@gmail.com_

## Licencia:

**Colmena -SGTH es Software Libre licenciado bajo una licencia [Creative Commons][fd7cfbc1]**  
![[Creative Commons](http://creativecommons.org/licenses/by-nc/4.0/)](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)  
Licencia Creative Commons Atribución-NoComercial 4.0 Internacional.
  [fd7cfbc1]: http://creativecommons.org/licenses/by-nc/4.0/ "Creative Commons 4.0"

-------
## Creditos:

- Frameworks:
    - [Laravel](https://laravel.com/)
    - [Bootstrap](http://getbootstrap.com)
- Plantilla HTML: [WebThemez](http://webthemez.com)
